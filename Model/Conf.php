<?php

  class Conf{
    static private $debug = True;

    static private $database = array
    (
      "hostname" => "localhost",
      "database" => "lumaBase",
      "login" => "luma",
      "password" => "luma",
    );

     public static function getLogin()
    {
      return self::$database["login"];
    }

    static public function getHost()
    {
      return self::$database["hostname"];
    }

    static public function getDB()
    {
      return self::$database["database"];
    }

    static public function getPasswd()
    {
      return self::$database["password"];
    }

    static public function getDebug()
    {
      return self::$debug ;
    }


  }
