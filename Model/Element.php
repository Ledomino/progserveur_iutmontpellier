<?php

  class Element{

    private $tag;
    private $child = array();
    private $content ;
    private $classCSS;


    public function __construct($tag , $class )
    {
      $this->tag = $tag;
      $this->classCSS = $class;


    }


    public function addChild($element)
    {
      array_push($this->child,$element);
    }

    public function contenu($content)
    {
        $this->content = $content;
    }

    public function getChild()
    {
      return $this->child;
    }

    public function getCodeHTML()
  {
    $disp =  "<{$this->tag} class={$this->classCSS}>";
    $tabSize = count($this->child);
    $cpt = 0;

    if($tabSize != 0)
    {
      while($cpt < $tabSize)
      {
        $disp .= $this->child[$cpt]->getCodeHTML();
        $cpt = $cpt + 1;
      }
      $disp .="</{$this->tag}>";

    }else {
      $disp .= $this->content ."</{$this->tag}>";
    }

    return $disp;
  }


  }


 ?>
