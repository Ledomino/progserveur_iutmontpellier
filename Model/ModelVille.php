<?php
  class ModelVille{
    private $nomVille;
    private $codepostal;
    private $longitude;
    private $latitude;
    private $idVille;
    private $maire;

    public function __construct($nVille = null, $cp = null, $logitude = null, $latitude = null, $maire = null)
    {
      if(isset($nVille))
      {
        $this->nomVille = $nVille;
      }

    }

    public  function getNomVille()
    {
      return $this->nomVille ;
    }

    public  function getCodePostal()
    {
      return $this->codepostal ;
    }

    public function getID()
    {
      return $this->idVille;
    }

    public function save()
    {
      $sql = "Insert into ";
    }

    public static function getAllVille()
    {
      $sql =  "Select nomVille from Ville";
    $rep = Model::$pdo->query($sql);
    $rep->setFetchMode(PDO::FETCH_CLASS, 'ModelVille');
    return $rep->fetchAll();

    }

    public static function getVilleByCP($cp)
    {
      $sql = "SELECT * from Ville WHERE codepostal=:nom_tag";
    // Préparation de la requête
    $req_prep = Model::$pdo->prepare($sql);

    $values = array(
        "nom_tag" => $cp,
    );
    // On donne les valeurs et on exécute la requête
    $req_prep->execute($values);
    var_dump($req_prep);
    $req_prep->setFetchMode(PDO::FETCH_CLASS,'Ville');
    $tab_ville = $req_prep->fetchAll();
    return $tab_ville;

    }

    public static function getVilleByID($id)
    {
      $sql = "SELECT * from Ville WHERE idVille=:nom_tag";
    // Préparation de la requête
    $req_prep = Model::$pdo->prepare($sql);

    $values = array(
        "nom_tag" => $id,
    );
    // On donne les valeurs et on exécute la requête
    $req_prep->execute($values);
    var_dump($req_prep);
    $req_prep->setFetchMode(PDO::FETCH_CLASS,'Ville');
    $tab_ville = $req_prep->fetchAll();
    return $tab_ville;

    }


  }
