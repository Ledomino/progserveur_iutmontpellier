<?php
  require_once(Model::$PATH."Require.php");
 ?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.1.1">
    <title><?php echo $pageTitle?></title>
    <link href="Vue/Template/bootstrap.min.css" rel="stylesheet">
    <link href="Vue/Template/style.css" rel="stylesheet">
  </head>
  <body>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
  <a class="navbar-brand" href="#">Navbar</a>
  </button>
</nav>

<main role="main" class="container">

  <div class="starter-template" style="padding-top:100px;">
    <?php
      include($pageBody);
    ?>
  </div>

</main><!-- /.container -->
</body>
</html>
