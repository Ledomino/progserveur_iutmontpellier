<?php
  require_once(Model::$PATH."Require.php");
 ?>

<!DOCTYPE html>

<html>
  <head>
    <title><?php echo $pageTitle?></title>
    <link href="<?php echo Model::$TEMPLATE; ?>" rel="stylesheet">
  </head>
  <body>
    <nav>
    </nav>
    <?php
      include($pageBody);
    ?>
  </body>
</html>
