<?php
  require_once(Model::$PATH."Require.php");
 ?>

<html>
<head>
  <title><?php echo $pageTitle;?></title>
  <link href="Vue/Template/style2.css" rel="stylesheet">
<body>
  <nav>
      <ul id="nav-list">
          <li class="nav-item"> <a href="#"> Home</a> </li>
          <li class="nav-item"> <a href="#"> link</a> </li>
      </ul>
  </nav>

  <main role="main" id="container-main">
     <div id="starter-template">
       <?php
       include($pageBody);
       ?>
     </div>
  </main>

</body>
</html>
